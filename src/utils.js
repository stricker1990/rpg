function randomFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function randomArrayElement(arr){
    if(arr.length === 0){
        return null;
    }
    const randomIndex = randomFromInterval(0, arr.length-1);
    return arr[randomIndex];
}

module.exports = {
    randomFromInterval,
    randomArrayElement
}