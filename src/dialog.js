const readLineSync = require('readline-sync');

function questionMaxHealth(){
    return readLineSync.questionInt('Введите максимальное здоровье персонажа: ');
}

function questionStartNewGame(){
    return readLineSync.keyInYN('Начать новую игру?');
}

module.exports = {
    questionMaxHealth,
    questionStartNewGame
};