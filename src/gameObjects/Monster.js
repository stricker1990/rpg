const {Creature} = require('./Creature');

class Monster extends Creature {
    constructor(){
        const moves = [];
        moves.push({
            name: "Удар когтистой лапой",
            physicalDmg: 3, // физический урон
            magicDmg: 0,    // магический урон
            physicArmorPercents: 20, // физическая броня
            magicArmorPercents: 20,  // магическая броня
            cooldown: 0
        });
        moves.push({
            name: "Огненное дыхание",
            physicalDmg: 0,
            magicDmg: 4,
            physicArmorPercents: 0,
            magicArmorPercents: 0,
            cooldown: 3
        });
        moves.push({
            name: "Удар хвостом",
            physicalDmg: 2,
            magicDmg: 0,
            physicArmorPercents: 50,
            magicArmorPercents: 0,
            cooldown: 2
        });
        super(10, 'Лютый', moves);
    }
}

module.exports = {
    Monster
};