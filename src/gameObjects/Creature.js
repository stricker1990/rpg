
const {randomArrayElement} = require('../utils');

class Creature {

    constructor(maxHealth, name, moves){

        this.maxHealth=maxHealth;

        this.name=name;

        this.moves=moves.map(move => Object.assign({currentCooldown: 0}, move));

        this.currentHealth=this.maxHealth;
        this.usedMoves = [];

        this.currentMove = null;
    }

    static calculateDamage(atackDamage = 0, defendPercent = 0){
        return Math.round(atackDamage * (100 - defendPercent)/100);
    }

    static getMoveInfo(move){
        return `${move.name} (физический урон - ${move.physicalDmg}, магический урон - ${move.magicDmg}, физическая защита - ${move.physicArmorPercents}%, магическая защита - ${move.magicArmorPercents}%, Ходов перезарядки: ${move.cooldown})`;
    }

    static skippingMove(){
        return {
            name: "Пропуск хода",
            physicalDmg: 0,
            magicDmg: 0,
            physicArmorPercents: 0,
            magicArmorPercents: 0,
            cooldown: 0,
            currentCooldown: 0
        };
    }

    infoHP(){
        return `${this.name }HP: ${this.currentHealth}/${this.maxHealth}`;
    }

    choseMove() {
        this.currentMove = this.choseFromUsableMoves(this.getUsableMoves());
        if(this.currentMove === null){
            this.currentMove = Creature.skippingMove();
        }
        console.log(`${this.name} применяет ${Creature.getMoveInfo(this.currentMove)}`);
    }

    choseFromUsableMoves(usableMoves){
        return randomArrayElement(usableMoves);
    }

    atack(gameObject){

        const atackMove = this.currentMove;
        const defendMove = gameObject.currentMove;

        const physicalDamage = Creature.calculateDamage(atackMove.physicalDmg, defendMove.physicArmorPercents);
        console.log(`${this.name} наносит ${atackMove.physicalDmg} физического урона. ${gameObject.name} блокирует ${defendMove.physicArmorPercents}%`);

        const magicDamage = Creature.calculateDamage(atackMove.magicDmg, defendMove.magicArmorPercents);
        console.log(`${this.name} наносит ${atackMove.magicDmg} магического урона. ${gameObject.name} блокирует ${defendMove.magicArmorPercents}%`);

        const fullDamage = physicalDamage + magicDamage;

        gameObject.currentHealth = gameObject.currentHealth - fullDamage;
        console.log(`${gameObject.name} теряет ${fullDamage} единиц здоровья. Осталось ${this.currentHealth}`);

        atackMove.currentCooldown = atackMove.cooldown;
        defendMove.currentCooldown = defendMove.cooldown;

    }

    updateCooldowns(){
        this.moves.forEach(move => {
            if(move.currentCooldown>0){
                move.currentCooldown--;
            }
        });
    }

    cleanCurrentMove(){
        this.currentMove = null;
    }

    update(){
        this.updateCooldowns();
        this.cleanCurrentMove();
    }

    getUsableMoves(){
        return this.moves.filter(move => move.currentCooldown === 0);
    }

}

module.exports ={
    Creature
};