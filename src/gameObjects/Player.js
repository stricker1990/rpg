const readlineSync = require('readline-sync');

const {Creature} = require('./Creature');

class Player extends Creature {
    constructor(maxHealth) {
        const moves = [];

        moves.push({
            name: "Удар боевым кодилом",
            physicalDmg: 2,
            magicDmg: 0,
            physicArmorPercents: 0,
            magicArmorPercents: 50,
            cooldown: 0
        });

        moves.push({
            name: "Вертушка левой пяткой",
            physicalDmg: 4,
            magicDmg: 0,
            physicArmorPercents: 0,
            magicArmorPercents: 0,
            cooldown: 4
        });

        moves.push({
            name: "Каноничный фаербол",
            physicalDmg: 0,
            magicDmg: 5,
            physicArmorPercents: 0,
            magicArmorPercents: 0,
            cooldown: 3
        });

        moves.push({
            name: "Магический блок",
            physicalDmg: 0,
            magicDmg: 0,
            physicArmorPercents: 100,
            magicArmorPercents: 100,
            cooldown: 4
        });

        super(maxHealth, 'Боевой маг Евстафий', moves);
    }

    choseFromUsableMoves(usableMoves){
        const movesIndex = readlineSync.keyInSelect(usableMoves.map(move => Creature.getMoveInfo(move)), "Выберите действие");
        if(movesIndex === -1){
            if(readlineSync.keyInYN('Выйти из игры? (Если выберете N - пропустите ход')){
                process.exit();
            }
            return null;
        }
        return usableMoves[movesIndex];
    }
};

module.exports = {
    Player
};