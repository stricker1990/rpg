const {
    questionMaxHealth,
    questionStartNewGame
} = require('./src/dialog');

const {Monster} = require('./src/gameObjects/Monster');
const {Player} = require('./src/gameObjects/Player');

function startGame(){
    const userMaxHealth = questionMaxHealth();

    const {player, monster} = initUnits(userMaxHealth);

    let roundsCount = 1;

    while(!isEndGame(player, monster)){
        console.log(`\nРАУНД ${roundsCount}\n`);
        console.log(`${monster.infoHP()}\n${player.infoHP()}\n`);

        monster.choseMove();
        player.choseMove();

        console.log('\n');

        monster.atack(player);
        player.atack(monster);

        monster.update();
        player.update();

        roundsCount++;
    }

    printWinner(monster, player);
}

function initUnits(userMaxHealth){
    return {
        player: new Player(userMaxHealth),
        monster: new Monster()
    }
}

function isEndGame(monster, player){
    return (monster.currentHealth<=0 || player.currentHealth<=0);
}

function printWinner(monster, player){
    if(monster.currentHealth<=0 && player.currentHealth<=0){
        console.log(`Ничья! ${monster.name} : ${monster.currentHealth}, ${player.name} : ${player.currentHealth}`);
    }else if(monster.currentHealth<=0){
        console.log(`Выиграл ${player.name}`);
    }else if(player.currentHealth<=0){
        console.log(`Выиграл ${monster.name}`);
    }
}

while(true){

    startGame();

    if(!questionStartNewGame()){
        break;
    }
}